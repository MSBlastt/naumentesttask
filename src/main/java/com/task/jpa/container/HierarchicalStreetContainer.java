package com.task.jpa.container;

import com.task.PhoneBookApplication;
import com.task.domain.Street;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.JPAContainerFactory;
import com.vaadin.addon.jpacontainer.provider.CachingLocalEntityProvider;


public class HierarchicalStreetContainer extends JPAContainer<Street> {

    public HierarchicalStreetContainer() {
        super(Street.class);
        setEntityProvider(new CachingLocalEntityProvider<>(
                Street.class,
                JPAContainerFactory
                        .createEntityManagerForPersistenceUnit(PhoneBookApplication.PERSISTENCE_UNIT)));
        setParentProperty("parent");
    }

    @Override
    public boolean areChildrenAllowed(final Object itemId) {
        return super.areChildrenAllowed(itemId)
                && getItem(itemId).getEntity().getCityName();
    }
}
