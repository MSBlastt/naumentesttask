package com.task.domain;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

@Entity
public class Street {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @OneToMany(mappedBy = "street")
    private Set<Person> persons;

    @Transient
    private Boolean cityName;

    @ManyToOne
    private Street parent;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Person> getPersons() {
        return persons;
    }

    public void setPersons(Set<Person> persons) {
        this.persons = persons;
    }

    public Street getParent() {
        return parent;
    }

    public void setParent(Street parent) {
        this.parent = parent;
    }

    public boolean getCityName() {
        if (cityName == null) {
            cityName = getPersons().size() == 0;
        }
        return cityName;
    }

    @Transient
    public String getHierarchicalName() {
        if (parent != null) {
            return parent.toString() + " : " + name;
        }
        return name;
    }

    @Override
    public String toString() {
        return getHierarchicalName();
    }
}
