package com.task.ui.editor;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Arrays;

import com.task.ui.selector.StreetSelector;
import com.vaadin.data.Item;
import com.vaadin.data.validator.BeanValidator;
import com.task.domain.Person;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.DefaultFieldFactory;
import com.vaadin.ui.Field;
import com.vaadin.ui.Form;
import com.vaadin.ui.FormFieldFactory;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Window;


public class PersonEditor extends Window implements Button.ClickListener,
        FormFieldFactory {

    private final Item personItem;
    private Form editorForm;
    private Button saveButton;
    private Button cancelButton;

    public PersonEditor(final Item personItem) {
        this.personItem = personItem;
        editorForm = new Form();
        editorForm.setFormFieldFactory(this);
        editorForm.setBuffered(true);
        editorForm.setImmediate(true);
        editorForm.setItemDataSource(personItem, Arrays.asList("lastName",
                "firstName", "patronymic", "phoneNumber", "street"));
        updateFieldsRu();
        saveButton = new Button("Сохранить", this);
        cancelButton = new Button("Отмена", this);

        editorForm.getFooter().addComponent(saveButton);
        editorForm.getFooter().addComponent(cancelButton);
        setSizeUndefined();
        setContent(editorForm);
        setCaption(buildCaption());
    }

    private String buildCaption() {
        if (personItem.getItemProperty("firstName").getValue() != null) {
            return String.format("%s %s %s",
                    personItem.getItemProperty("lastName").getValue(),
                    personItem.getItemProperty("firstName").getValue(),
                    personItem.getItemProperty("patronymic").getValue());
        }
        else return "Создание новой записи";
    }

    private void updateFieldsRu() {
        for (Object fieldName : editorForm.getItemPropertyIds()) {
            if (!"street".equals(fieldName)) {
                Field<?> field = editorForm.getField(fieldName);
                field.setRequiredError("Обязательное поле!");
                field.setRequired(true);
            }
        }
        editorForm.getField("lastName").setCaption("Фамилия");
        editorForm.getField("firstName").setCaption("Имя");
        editorForm.getField("patronymic").setCaption("Отчество");
        editorForm.getField("phoneNumber").setCaption("Номер телефона");

    }

    @Override
    public void buttonClick(ClickEvent event) {
        if (event.getButton() == saveButton) {
            editorForm.commit();
            fireEvent(new EditorSavedEvent(this, personItem));
        } else if (event.getButton() == cancelButton) {
            editorForm.discard();
        }
        close();
    }

    @Override
    public Field createField(Item item, final Object propertyId, final Component uiContext) {
        Field field = DefaultFieldFactory.get().createField(item, propertyId,
                uiContext);
        if ("street".equals(propertyId)) {
            field = new StreetSelector();
        } else if (field instanceof TextField) {
            ((TextField) field).setNullRepresentation("");
        }

        field.addValidator(new BeanValidator(Person.class, propertyId
                .toString()));

        return field;
    }

    public void addListener(final EditorSavedListener listener) {
        try {
            Method method = EditorSavedListener.class.getDeclaredMethod(
                    "editorSaved", EditorSavedEvent.class);
            addListener(EditorSavedEvent.class, listener, method);
        } catch (final java.lang.NoSuchMethodException e) {
            throw new java.lang.RuntimeException(
                    "Internal error");
        }
    }

    public void removeListener(EditorSavedListener listener) {
        removeListener(EditorSavedEvent.class, listener);
    }

    public static class EditorSavedEvent extends Component.Event {

        private Item savedItem;

        public EditorSavedEvent(final Component source, final Item savedItem) {
            super(source);
            this.savedItem = savedItem;
        }

        public Item getSavedItem() {
            return savedItem;
        }
    }

    public interface EditorSavedListener extends Serializable {
        void editorSaved(EditorSavedEvent event);
    }

}
