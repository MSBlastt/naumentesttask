package com.task.ui.selector;

import com.task.PhoneBookApplication;
import com.task.domain.Street;
import com.vaadin.addon.jpacontainer.EntityItem;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.JPAContainerFactory;
import com.vaadin.data.Property;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.data.util.filter.Compare.Equal;
import com.vaadin.data.util.filter.IsNull;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.CustomField;


public class StreetSelector extends CustomField<Street> {
    private ComboBox geographicalStreet = new ComboBox();
    private ComboBox street = new ComboBox();

    private JPAContainer<Street> container;
    private JPAContainer<Street> geoContainer;

    public StreetSelector() {
        container = JPAContainerFactory.make(Street.class,
                PhoneBookApplication.PERSISTENCE_UNIT);
        geoContainer = JPAContainerFactory.make(Street.class,
                PhoneBookApplication.PERSISTENCE_UNIT);
        setCaption("Адрес");
        geoContainer.addContainerFilter(new IsNull("parent"));
        geographicalStreet.setContainerDataSource(geoContainer);
        geographicalStreet.setItemCaptionPropertyId("name");
        geographicalStreet.setImmediate(true);
        geographicalStreet.setNewItemsAllowed(false);
        container.setApplyFiltersImmediately(false);
        filterStreets(null);
        street.setContainerDataSource(container);
        street.setItemCaptionPropertyId("name");
        street.setNewItemsAllowed(false);

        geographicalStreet.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(
                    final com.vaadin.data.Property.ValueChangeEvent event) {
                EntityItem<Street> item = geoContainer
                        .getItem(geographicalStreet.getValue());
                Street entity = item.getEntity();
                filterStreets(entity);
            }
        });
        street.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(
                    com.vaadin.data.Property.ValueChangeEvent event) {

                if (street.getValue() == null) {
                    setValue(null, false);
                } else {
                    Street entity = container
                            .getItem(street.getValue()).getEntity();
                    setValue(entity, false);
                }
            }
        });
    }

    @Override
    protected Component initContent() {
        CssLayout cssLayout = new CssLayout();
        cssLayout.addComponent(geographicalStreet);
        cssLayout.addComponent(street);
        return cssLayout;
    }

    private void filterStreets(Street currentGeoStreet) {
        if (currentGeoStreet == null) {
            street.setValue(null);
            street.setEnabled(false);
        } else {
            container.removeAllContainerFilters();
            container.addContainerFilter(new Equal("parent",
                    currentGeoStreet));
            container.applyFilters();
            street.setValue(null);
            street.setEnabled(true);
        }
    }

    @Override
    public void setPropertyDataSource(Property newDataSource) {
        super.setPropertyDataSource(newDataSource);
        setStreet((Street) newDataSource.getValue());
    }

    @Override
    public void setValue(Street newValue) throws ReadOnlyException,
            Converter.ConversionException {
        super.setValue(newValue);
        setStreet(newValue);
    }

    private void setStreet(Street street) {
        geographicalStreet.setValue(street != null ? street
                .getParent().getId() : null);
        this.street
                .setValue(street != null ? street.getId() : null);
    }

    @Override
    public Class<? extends Street> getType() {
        return Street.class;
    }

}
