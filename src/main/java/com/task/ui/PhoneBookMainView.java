package com.task.ui;

import com.task.jpa.container.HierarchicalStreetContainer;
import com.task.PhoneBookApplication;
import com.task.domain.Street;
import com.task.domain.Person;
import com.task.ui.editor.PersonEditor;
import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.JPAContainerFactory;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.filter.Compare.Equal;
import com.vaadin.data.util.filter.Like;
import com.vaadin.data.util.filter.Or;
import com.vaadin.event.FieldEvents.TextChangeEvent;
import com.vaadin.event.FieldEvents.TextChangeListener;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;
import org.vaadin.dialogs.ConfirmDialog;

public class PhoneBookMainView extends HorizontalSplitPanel{

    private Table personTable;

    private Button deleteButton;
    private Button editButton;
    private Button resetFiltersButton;

    private JPAContainer<Street> streets;
    private JPAContainer<Person> persons;

    private Street streetFilter;
    private String textFilter;

    private Tree groupTree;

    public PhoneBookMainView() {
        streets = new HierarchicalStreetContainer();
        persons = JPAContainerFactory.make(Person.class,
                PhoneBookApplication.PERSISTENCE_UNIT);
        buildTree();
        buildMainArea();

        setSplitPosition(30);
    }

    private void buildMainArea() {
        VerticalLayout verticalLayout = new VerticalLayout();
        setSecondComponent(verticalLayout);

        personTable = new Table(null, persons);
        personTable.setSelectable(true);
        personTable.setImmediate(true);
        personTable.addValueChangeListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(ValueChangeEvent event) {
                setModificationsEnabled(event.getProperty().getValue() != null);
            }

            private void setModificationsEnabled(boolean b) {
                deleteButton.setEnabled(b);
                editButton.setEnabled(b);
            }
        });

        personTable.setSizeFull();
        personTable.addItemClickListener(new ItemClickListener() {
            @Override
            public void itemClick(ItemClickEvent event) {
                if (event.isDoubleClick()) {
                    personTable.select(event.getItemId());
                    editButton.click();
                }
            }
        });

        personTable.setVisibleColumns("lastName",
                "firstName", "patronymic", "phoneNumber", "street");
        personTable.setColumnHeaders("Имя",
                "Фамилия", "Отчество", "Номер Телефона", "Адрес");

        HorizontalLayout toolbar = new HorizontalLayout();
        Button newButton = new Button("Добавить");
        newButton.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
                final BeanItem<Person> newPersonItem = new BeanItem<>(
                        new Person());
                PersonEditor personEditor = new PersonEditor(newPersonItem);
                personEditor.addListener(new PersonEditor.EditorSavedListener() {
                    @Override
                    public void editorSaved (final PersonEditor.EditorSavedEvent event) {
                        persons.addEntity(newPersonItem.getBean());
                    }
                });
                UI.getCurrent().addWindow(personEditor);
            }
        });

        deleteButton = new Button("Удалить");
        deleteButton.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(ClickEvent event) {
                ConfirmDialog confirmDialog = new ConfirmDialog();
                confirmDialog.show(getUI(), "Подтвердите действие",
                        "Действительно удалить запись?", "Да", "Нет",
                        new ConfirmDialog.Listener() {
                    @Override
                    public void onClose(ConfirmDialog confirmDialog) {
                        if (confirmDialog.isConfirmed()) {
                            persons.removeItem(personTable.getValue());
                            deleteButton.setEnabled(false);
                            editButton.setEnabled(false);
                        }
                    }
                });

            }
        });
        deleteButton.setEnabled(false);

        editButton = new Button("Редактировать");
        editButton.addClickListener(new Button.ClickListener() {

            @Override
            public void buttonClick(final ClickEvent event) {
                UI.getCurrent().addWindow(
                        new PersonEditor(personTable.getItem(personTable
                                .getValue())));
            }
        });
        editButton.setEnabled(false);

        final TextField searchField = new TextField();
        searchField.setInputPrompt("Поиск по имени или фамилии");
        searchField.setWidth("300");
        searchField.addTextChangeListener(new TextChangeListener() {

            @Override
            public void textChange(TextChangeEvent event) {
                textFilter = event.getText();
                updateSearchFilters();
            }
        });


        resetFiltersButton = new Button("Сбросить фильтры");
        resetFiltersButton.setEnabled(false);
        resetFiltersButton.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(ClickEvent clickEvent) {
                textFilter = "";
                searchField.setValue("");
                resetAllFilters();
                resetFiltersButton.setEnabled(false);
                for (Object row : groupTree.getItemIds()) {
                    groupTree.collapseItem(row);
                }
            }
        });

        toolbar.addComponent(newButton);
        toolbar.addComponent(deleteButton);
        toolbar.addComponent(editButton);
        toolbar.addComponent(resetFiltersButton);
        toolbar.addComponent(searchField);
        toolbar.setWidth("100%");
        toolbar.setExpandRatio(searchField, 1);
        toolbar.setComponentAlignment(searchField, Alignment.TOP_RIGHT);

        verticalLayout.addComponent(toolbar);
        verticalLayout.addComponent(personTable);
        verticalLayout.setExpandRatio(personTable, 1);
        verticalLayout.setSizeFull();

    }

    private void buildTree() {
        groupTree = new Tree(null, streets);
        groupTree.setItemCaptionPropertyId("name");

        groupTree.setImmediate(true);
        groupTree.setSelectable(true);
        groupTree.addValueChangeListener(new Property.ValueChangeListener() {

            @Override
            public void valueChange(final ValueChangeEvent event) {
                Object id = event.getProperty().getValue();
                if (id != null) {
                    streetFilter = streets.getItem(id).getEntity();
                } else if (streetFilter != null) {
                    streetFilter = null;
                }
                updateAddressFilters();
                editButton.setEnabled(false);
                deleteButton.setEnabled(false);
                resetFiltersButton.setEnabled(true);
            }

        });
        setFirstComponent(groupTree);
    }

    private void updateAddressFilters() {
        persons.setApplyFiltersImmediately(false);
        persons.removeAllContainerFilters();
        if (streetFilter != null) {
            if (streetFilter.getParent() == null) {
                persons.addContainerFilter(new Equal("street.parent",
                        streetFilter));
            } else {
                persons.addContainerFilter(new Equal("street",
                        streetFilter));
            }
        }
        if (textFilter != null) {
            Or or = new Or(new Like("firstName", textFilter + "%", false),
                    new Like("lastName", textFilter + "%", false));
            persons.addContainerFilter(or);
        }
        resetFiltersButton.setEnabled(true);
        persons.applyFilters();
    }

    private void updateSearchFilters() {
        persons.setApplyFiltersImmediately(false);
        persons.removeAllContainerFilters();
        if (textFilter != null) {
            Or or = new Or(new Like("firstName", textFilter + "%", false),
                    new Like("lastName", textFilter + "%", false));
            persons.addContainerFilter(or);
        }
        resetFiltersButton.setEnabled(true);
        persons.applyFilters();
    }

    private void resetAllFilters() {
        persons.setApplyFiltersImmediately(false);
        persons.removeAllContainerFilters();
        persons.applyFilters();
    }
}
