package com.task;

import com.task.utils.DemoDataGenerator;
import com.task.ui.PhoneBookMainView;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.UI;


public class PhoneBookApplication extends UI {

    public static final String PERSISTENCE_UNIT = "phonebook";

    static {
        DemoDataGenerator.create();
    }

    @Override
    protected void init(final VaadinRequest request) {
        setContent(new PhoneBookMainView());
    }
}
