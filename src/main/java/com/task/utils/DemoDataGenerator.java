package com.task.utils;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import com.task.domain.Street;
import com.task.domain.Person;

public class DemoDataGenerator {

	static final String[] FIRST_NAMES = {"Иван", "Петр", "Владимир", "Михаил",
			"Олег", "Николай", "Александр", "Даниил", "Фёдор", "Геннадий",
			"Роман", "Евгений", "Максим" };
	static final  String[] LAST_NAMES = {"Иванов", "Петров", "Сидоров", "Курчатов",
			"Ландау", "Смирнов", "Тряпичкин", "Козырьков", "Вениаминов",};
	static final  String[] PATRONYMICS = {"Михайлович", "Александрович", "Алексеевич", "Иванович",
			"Олегович", "Контстантинович", "Дмитриевич", "Семёнович", "Фёдорович", "Степанович",
			"Даниилович", "Николаевич"};
	static final  String CITIES[] = {"Москва", "Санкт-Петербург", "Новосибирск",
			"Екатеринбург", "Нижний Новгород", "Краснодар", "Уфа", "Владивосток", "Иркутск"};
	static final String STREETS[] = {"ул. Московская 217", "пр. Ленина 150",
			"ул. Коммунаров 147", "ул. Дзержинского 285", "ул. Маяковского 13","ул. Московская 5", "пр. Ленина 8",
			"ул. Коммунаров 56", "ул. Дзержинского 5", "ул. Маяковского 13", "ул. Московская 23", "пр. Ленина 88",
			"ул. Коммунаров 12", "ул. Дзержинского 16б", "ул. Маяковского 27"};

	public static void create() {

		EntityManager em = Persistence
				.createEntityManagerFactory("phonebook")
				.createEntityManager();

		em.getTransaction().begin();
		Random r = new Random(0);

		for (String city : CITIES) {
			Street geoGroup = new Street();
			geoGroup.setName(city);
			for (String streetName : STREETS) {
				Street street = new Street();
				street.setName(streetName);
				Set<Person> gPersons = new HashSet<>();
				int amount = r.nextInt(15) + 1;
				for (int i = 0; i < amount; i++) {
					Person person = new Person();
					person.setFirstName(FIRST_NAMES[r.nextInt(FIRST_NAMES.length)]);
					person.setLastName(LAST_NAMES[r.nextInt(LAST_NAMES.length)]);
					person.setPatronymic(PATRONYMICS[r.nextInt(PATRONYMICS.length)]);
					person.setPhoneNumber("+7 915 555 " + r.nextInt(10) + r.nextInt(10)
							+ r.nextInt(10) + r.nextInt(10));
					person.setStreet(street);
					gPersons.add(person);
					em.persist(person);
				}
				street.setParent(geoGroup);
				street.setPersons(gPersons);
				em.persist(street);
			}

			em.persist(geoGroup);
		}



		em.getTransaction().commit();
	}

}
